@extends('app')

@section('content')

        </div>
        <div class="page-title-area1 text-center" style="background-image: url({{asset('frontend/assets/img/head.jpg')}});">
        </div>
        <!--page-welcome-area start -->
        <!--tours-details-area-content start -->
        <div class="" style=""> 
            <div class="container">
                <div class="row" style="margin-top : 120px;">
                    <div class="col-md-1 "></div>
                    <div class="col-md-10 ">
                        <div class="tours-details-content-left">
                            <div class="tours-gallery-heading" >
                                <h2>{{ $wisata->judul }}</h2>
                            </div>
                            <div class="tours-details-gallery owl-carousel padding-bottom">
                                <div class="tours-details-gallery-single" data-dot="<img src='{{asset('images/'. $wisata->foto1 )}}' alt=''>">
                                    <img src="{{asset('images/'. $wisata->foto1 )}}" alt="">
                                </div>
                                <div class="tours-details-gallery-single" data-dot="<img src='{{asset('images/'. $wisata->foto2 )}}' alt=''>">
                                    <img src="{{asset('images/'. $wisata->foto2 )}}" alt="">
                                </div>
                                <div class="tours-details-gallery-single" data-dot="<img src='{{asset('images/'. $wisata->foto3 )}}' alt=''>">
                                    <img src="{{asset('images/'. $wisata->foto3 )}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 "></div>
                </div>
            </div>
            
            <div class="container">        
                <div class="row">
                    <div class="col-md-6">
                        <div class="tab-content tours-details-tab-content padding-top">
                                <div class="tours-details-menu-bottom">
                                    <h2>{{ $wisata->judul}}</h2>
                                    <p>{{ $wisata->konten}}</p>
                                </div>
                                <div class="tourist-details-information">
                                    <br>
                                    <h4>Alamat</h4>
                                    <p>{{$wisata->jalan}}, {{$wisata->kelkec}}, {{$wisata->kotakab}}, {{$wisata->provinsi}}</p>
                                </div>
                        </div> 
                    </div>
                    <div class="col-md-6" style="margin-top : 100px;">
                        <iframe src="{{ $wisata->peta }}" width="600" height="450" frameborder="0" style="border:0" allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
        </div>

@endsection