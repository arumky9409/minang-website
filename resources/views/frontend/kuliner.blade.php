@extends('app')
@section('content')

            

<!-- content -->
            <div class="page-title-area black-overlay text-center" style="background-image: url({{asset('frontend/assets/img/kuliner.jpg')}});">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title-inner">
                                <div class="page-title-inner-table-cell">
                                    <h1>Kuliner</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- background-image: url(../img/bghead2.jpg); -->

        <div class="destinations-page-content-area section-padding">
                <div class="container">
                    <div class="row">
                    @foreach($kuliner as $k)
                        <div class="col-md-3 col-sm-6" style="margin-bottom : 15px; margin-top : 45px;">
                            <div class="single-top-destination-item black-overlay" style="background-image : url({{ asset('images/'.$k->foto1) }});">
                                <div class="single-top-destination-item-inner">
                                    <h2>{{ $k->judul }}</h2>
                                    <a href="{{ url('detail/'.$k->id) }}" class="pink-btn">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            
        </div>
        <!-- content end -->


@endsection