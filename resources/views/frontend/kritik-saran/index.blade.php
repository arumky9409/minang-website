@extends('app')



@section('content')



        <!-- content -->

        <div class="page-title-area black-overlay text-center" style="background-image: url({{asset('frontend/assets/img/ks.jpg')}});">

                <div class="container">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="page-title-inner">

                                <div class="page-title-inner-table-cell">

                                    <h1>Kontak</h1>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



        <div class="contact-page-conent-area section-padding">

            <div class="container">

                <div class="row">

                    <div class="col-md-12 col-sm-7">

                        <div class="contact-page-form">

                            <p><b>Silahkan isi form di bawah jika ingin menyampaikan pesan berupa kritik dan saran yang membangun website ini menjadi lebih baik. Anda juga dapat mengirim pesan berupa pengalaman Anda selama berwisata di Sumatera Barat &#128515; </b></p>

                            <form action="{{route('storePesan')}}" method="POST">

                            {{csrf_field()}}

                                <div class="row">

                                    <div class="col-md-6">

                                        <label for="contact-us-name">Nama*</label>

                                        <input id="contact-us-name" name="nama" type="text">

                                    </div>

                                    <div class="col-md-6">

                                        <label for="contact-us-email">Email*</label>

                                        <input id="contact-us-email" name="email" type="email">

                                    </div>

                                </div>

                                <label for="contact-us-message">Pesan*</label>

                                <textarea name="pesan" id="contact-us-message" cols="30" rows="10"></textarea>

                                <button type="submit" class="pink-btn">Kirim</button>

                            </form>

                        </div> 

                    </div>

                </div>

            </div>

        </div>

        <!-- content end -->



@endsection