@extends('admin.master')

@section('content')





    <!-- title -->

    <div class="row">

        <div class="col-12 d-flex no-block align-items-center">

            <h3 class="page-title">Penilaian</h3>

        </div>

    </div>

    

    <div class="container-fluid">

        <!-- Sales Cards  -->

        <div class="row">

            <div class="col-md-6 col-lg-3 col-xlg-3">

                <div class="card card-hover">

                    <div class="box bg-success text-center text-white">

                        <p style="font-size:50px">&#128515;</p>

                        <h5>Sangat Baik</h5>

                        <h1><span class="counter">{{$sangat_baik}}</span></h1>

                    </div>

                </div>

            </div>

            <!-- Column -->

            <div class="col-md-6 col-lg-3 col-xlg-3">

                <div class="card card-hover">

                    <div class="box bg-cyan text-center text-white">

                        <!-- <h1><i class="fas fa-smile"></i></h1> -->

                        <p style="font-size:50px">&#128578;</p>

                        <h5>Baik</h5>

                        <h1><span class="counter">{{$baik}}</span></h1>

                    </div>

                </div>

            </div>

            <!-- Column -->

            <div class="col-md-6 col-lg-3 col-xlg-3">

                <div class="card card-hover">

                    <div class="box bg-warning text-center text-white">

                        <!-- <h1><i class="fas fa-meh"></i></h1> -->

                        <p style="font-size:50px">&#128528;</p>

                        <h5>Cukup</h5>

                        <h1><span class="counter">{{$cukup}}</span></h1>

                    </div>

                </div>

            </div>

            <!-- Column -->

            <div class="col-md-6 col-lg-3 col-xlg-3">

                <div class="card card-hover">

                    <div class="box bg-danger text-center text-white">

                        <!-- <h1><i class="fas fa-frown"></i></h1> -->

                        <p style="font-size:50px">&#128532;</p>

                        <h5>Kurang</h5>

                        <h1><span class="counter">{{$kurang}}</span></h1>

                    </div>

                </div>

            </div>

        </div>

        

        <div class="row">

            <div class="col-md-6 col-lg-12 col-xlg-3">

                <div class="card card-hover">

                    <div class="box text-center text-black">

                        <h1>Total Penilaian</h1>

                        <h1><span class="counter">{{$total}}</span></h1>

                    </div>

                </div>

            </div>

        </div>



@endsection