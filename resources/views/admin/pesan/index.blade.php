@extends('admin.master')

@section('content')



<div class="card">

    <div class="card-body">

        <h5 class="card-title m-b-0">Pesan Pengunjung</h5>

    </div>

    <table class="table">

        <thead>

            <tr>
            <th scope="col" class="text-center"><strong>ID</strong></th>
            <th scope="col" class="text-center"><strong>Nama Pengunjung</strong></th>
            <th scope="col" class="text-center"><strong>Email</strong></th>
            <th scope="col" class="text-center"><strong>Pesan</strong></th>
            <th scope="col" class="text-center"><strong>Tampilkan</strong></th>
            <th scope="col" class="text-center"><strong>Aksi</strong></th>
            </tr>

        </thead>



        <tbody>

        @foreach($pesans as $pesan)

            <tr>

            <td scope="row" class="text-center">{{$pesan->id}}</td>

            <td class="text-center">{{$pesan->nama}}</td>

            <td class="text-center">{{$pesan->email}}</td>

            <td class="text-center">{{$pesan->pesan}}</td>

            <td class="text-center">

                @if($pesan->validation==0)

                    <a href="{{route('validation',['id'=>$pesan->id])}}" class="fas fa-check-square"></i></a>

                @else

                    <a href="{{route('validationNo',['id'=>$pesan->id])}}"class="fas fa-window-close"></i></a>

                @endif

            </td>



            <td>                       

                <form onsubmit = "return confirm('Apakah yakin pesan ini dihapus?');" action="{{ url('admin/pesan/'. $pesan->id ) }}" method="get">

                    @csrf

                    <input type="hidden" name="_method" value="DELETE">

                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>

                </form>



                <!-- <form action="{{ url('pesan/'. $pesan->id ) }}" method="post">

                    @csrf

                    <input type="hidden" name="_method" value="DELETE">

                    <button type="submit" class="btn btn-danger">Hapus</button>

                </form> -->

            </td>

            </tr>

        @endforeach

        </tbody>

    </table>

</div>

@endsection