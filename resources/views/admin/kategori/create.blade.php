@extends('admin.master')

@section('content')

<div class="card">

    <form action="{{route('storeKategori')}}" method="post" class="form-horizontal">

    {{csrf_field()}} 

    <div class="card-body">

        <h4 class="card-title">Input Kategori Wisata</h4>

        <div class="form-group row">

        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Kategori</label>

        <div class="col-sm-9">

        <input type="text" class="form-control" id="fname" placeholder="Kategori Wisata" name="kategori">

        </div>

        </div>



    </div>

    <div class="border-top">

        <div class="card-body">

            <button type="submit" class="btn btn-primary">Input</button>

        </div>

    </div>

    </form>

</div>

@endsection