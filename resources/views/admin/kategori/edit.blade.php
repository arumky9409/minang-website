@extends('admin.master')

@section('content')

<div class="card">

    <form action="{{url('kategori/'.$kategori->id)}}" method="post" class="form-horizontal">

    {{csrf_field()}} 

    <input type="hidden" name="_method" value="PUT">

    <div class="card-body">

        <h4 class="card-title">Ubah Kategori Wisata</h4>

        <div class="form-group row">

        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Kategori</label>

        <div class="col-sm-9">

            <input type="text" class="form-control" id="fname" value="{{ $kategori->nama_kat }}" placeholder="Nama Kategori" name="nama_kat">

        </div>

        </div>



    </div>

    <div class="border-top">

        <div class="card-body">

            <button type="submit" class="btn btn-primary">Ubah</button>

        </div>

    </div>

    </form>

</div>

@endsection