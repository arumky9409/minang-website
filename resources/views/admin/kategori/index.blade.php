@extends('admin.master')

@section('content')

<div class="card">

    <div class="card-body">

        <h5 class="card-title m-b-0">Daftar Kategori Wisata</h5>

    </div>

    <table class="table">

        <thead>

            <tr>

            <th scope="col" class="text-center"><strong>ID</strong></th>

            <th scope="col" class="text-center"><strong>Nama Kategori</strong></th>

            <th scope="col" class="text-center"><strong>Aksi</strong></th>

            </tr>

        </thead>

        <tbody>

        @foreach($kategoris as $kategori)

            <tr>

            <th scope="row" class="text-center">{{$kategori->id}}</th>

            <td class="text-center">{{$kategori->nama_kat}}</td>

            <td class="text-center">

                <a href="{{ url('kategori/'.$kategori->id.'/edit') }}" class="btn btn-info">Ubah</a>

            </td>

            </tr>

        @endforeach

        </tbody>

    </table>

</div>

@endsection