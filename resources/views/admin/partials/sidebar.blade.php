<aside class="left-sidebar" data-sidebarbg="skin5">

    <!-- Sidebar scroll-->

    <div class="scroll-sidebar">

        <!-- Sidebar navigation-->

        <nav class="sidebar-nav">

            <ul id="sidebarnav" class="p-t-30">

                <li class="sidebar-item">

                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/dashboard')}}" aria-expanded="false">

                        <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span>

                    </a>

                </li>



                <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fab fa-delicious"></i><span class="hide-menu"> Kategori Wisata</span></a>

                    <ul aria-expanded="false" class="collapse first-level">

                        <li class="sidebar-item">

                            <a href="{{route('createKategori')}}" class="sidebar-link">

                                <i class="far fa-edit"></i><span class="hide-menu"> Input Kategori Wisata </span>

                            </a>

                        </li>

                        <li class="sidebar-item">

                            <a href="{{route('indexKategori')}}" class="sidebar-link">

                                <i class="far fa-list-alt"></i><span class="hide-menu"> Daftar Kategori Wisata </span>

                            </a>

                        </li>

                    </ul>

                </li>



                <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-info"></i><span class="hide-menu"> Informasi Wisata</span></a>

                    <ul aria-expanded="false" class="collapse first-level">

                        <li class="sidebar-item">

                            <a href="{{route('createInformasi')}}" class="sidebar-link">

                                <i class="far fa-edit"></i><span class="hide-menu"> Input Informasi Wisata</span>

                            </a>

                        </li>

                        <li class="sidebar-item">

                            <a href="{{route('indexInformasi')}}" class="sidebar-link">

                                <i class="far fa-list-alt"></i><span class="hide-menu"> Daftar Informasi Wisata </span>

                            </a>

                        </li>

                    </ul>

                </li>



                <li class="sidebar-item">

                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('indexPesan')}}" aria-expanded="false">

                        <i class="mdi mdi-forum"></i><span class="hide-menu">Pesan Pengunjung</span>

                    </a>

                </li>

            </ul>

        </nav>

        <!-- End Sidebar navigation -->

    </div>

    <!-- End Sidebar scroll-->

</aside>