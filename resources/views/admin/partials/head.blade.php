<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Tell the browser to be responsive to screen width -->

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="description" content="">

<meta name="author" content="">

<!-- Favicon icon -->

<link rel="icon" type="image/png" sizes="16x16" href="{{asset('backend')}}/assets/images/iconwm.png">

<title>Admin - Wisata Minang</title>

<!-- Custom CSS -->

<link href="{{asset('vendor/backend')}}/libs/flot/css/float-chart.css" rel="stylesheet">

<!-- Custom CSS -->

<link href="{{asset('vendor/backend')}}/dist/css/style.min.css" rel="stylesheet">