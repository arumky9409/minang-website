<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    @include('admin.partials.head')
</head>

<body>

    <div id="main-wrapper">

        @include('admin.partials.header')
        @include('admin.partials.sidebar')
        
        <div class="page-wrapper">
            <div class="container-fluid">
            
                @yield('content')
                
            </div>
            <!-- End Container fluid  -->
            @include('admin.partials.footer')
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{asset('vendor/backend')}}/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('vendor/backend')}}/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{asset('vendor/backend')}}/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="{{asset('vendor/backend')}}/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="{{asset('vendor/backend')}}/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('vendor/backend')}}/dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="{{asset('vendor/backend')}}/dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="{{asset('vendor/backend')}}/libs/flot/excanvas.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/flot/jquery.flot.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/flot/jquery.flot.pie.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/flot/jquery.flot.time.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/flot/jquery.flot.stack.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="{{asset('vendor/backend')}}/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{asset('vendor/backend')}}/dist/js/pages/chart/chart-page-init.js"></script>

</body>

</html>