@extends('admin.master')

@section('content')

<div class="card">

    <div class="card-body">

        <h5 class="card-title m-b-0">Daftar Informasi Wisata</h5>

    </div>

    <table class="table">

        <thead>

            <tr>

            <th scope="col" class="text-center"><strong>ID</strong></th>

            <th scope="col" class="text-left"><strong>Nama Wisata</strong></th>

            <th scope="col" class="text-center"><strong>Kategori</strong></th>

            <th scope="col" class="text-jalan"><strong>Jalan</strong></th>

            <th scope="col" class="text-left"><strong>Kelurahan/Kecamatan</strong></th>

            <th scope="col" class="text-left"><strong>Kota/Kabupaten</strong></th>

            <th scope="col" colspan="2" class="text-center"><strong>Aksi</strong></th>

            </tr>

        </thead>

        <tbody>

        @foreach($informasis as $informasi)

            @php

                $nama_kat = App\Kategori::where('id', $informasi->id_kategori)->first();

            @endphp

            <tr>

            <td scope="row" class="text-center">{{$informasi->id}}</td>

            <td class="text-left">{{$informasi->judul}}</td>

            <td class="text-center">{{$nama_kat->nama_kat}}</td>

            <td class="text-left">{{$informasi->jalan}}</td>

            <td class="text-left">{{$informasi->kelkec}}</td>

            <td class="text-left">{{$informasi->kotakab}}</td>

            <td class="text-center">

                <a href="{{ url('informasi/'. $informasi->id .'/edit') }}" class="btn btn-info btn-sm">Ubah</a>

            </td>

            <td>

                <form onsubmit = "return confirm('Apakah yakin informasi {{$informasi->judul}} dihapus?')" class="d-inline" action="{{route('informasi.destroy',['id' => $informasi->id])}}" method="post">

                    @csrf

                    <input type="hidden" name="_method" value="DELETE">

                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>

                </form>

            </td>

            </tr>

        @endforeach

        </tbody>

    </table>



    <nav>

        {{$informasis->links()}}

    </nav>

    

    <!-- <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data Informasi</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                </div>

                <div class="modal-body">

                        Apakah yakin informasi {{$informasi->judul}} dihapus?

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>

                    <form action="{{ url('informasi/'. $informasi->id ) }}" method="post">

                        @csrf

                        <input type="hidden" name="_method" value="DELETE">

                        <button type="submit" class="btn btn-danger">Yakin</button>

                    </form>

                </div>

            </div>

        </div>

    </div> -->

</div>

@endsection