@extends('admin.master')

@section('content')



<div class="card">

<form action="{{url('informasi/'.$informasi->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">

    

@csrf

<input type="hidden" value='PUT' name='_method'>

        <div class="card-body">

            <h4 class="card-title">Ubah Informasi Wisata</h4>

            <div class="form-group row">

                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Wisata</label>

                <div class="col-sm-9">

                    <input type="text" value="{{ $informasi->judul }}" class="form-control" id="fname" placeholder="Nama Wisata" name="judul">

                </div>

            </div>

            <div class="form-group row">

                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Kategori</label>

                <div class="col-sm-9">

                    <select type="text" class="form-control" id="fname" name="id_kategori">

                        <option disabled selected>Pilih Kategori</option>

                        @foreach ($kategoris as $kategori)

                            <option @if($kategori->id == $informasi->id_kategori) selected @endif value="{{$kategori->id}}">{{$kategori->nama_kat}}</option>

                        @endforeach

                    </select>

                </div>

            </div>

            <div class="form-group row">

                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Alamat</label>

                <div class="col-sm-9">

                    <input type="text" value="{{ $informasi->jalan }}" class="form-control" id="lname" placeholder="Nama Jalan" name="jalan">

                </div>

            </div>

            <div class="form-group row">

                <label for="lname" class="col-sm-3 text-right control-label col-form-label"></label>

                <div class="col-sm-9">

                    <input type="text" value="{{ $informasi->kelkec }}" class="form-control" id="lname" placeholder="Kelurahan/Kecamatan" name="kelkec">

                </div>

            </div>

            <div class="form-group row">

                <label for="email1" class="col-sm-3 text-right control-label col-form-label"></label>

                <div class="col-sm-9">

                    <input type="text" class="form-control" value="{{ $informasi->kotakab }}" id="email1" placeholder="Kota/Kabupaten" name="kotakab">

                </div>

            </div>

            <div class="form-group row">

                <label for="cono1" class="col-sm-3 text-right control-label col-form-label"></label>

                <div class="col-sm-9">

                    <input type="text" class="form-control" id="cono1" placeholder="Provinsi" value="{{ $informasi->provinsi }}" name="provinsi" value="Sumatera Barat" readonly>

                </div>

            </div>

            <div class="form-group row">

                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Status</label>

                <div class="col-sm-9">

                    <input type="radio" name="populer" value="Populer">Populer  
                    <br>
                    <input type="radio" name="populer" value="Rare">Jarang dikunjungi

                </div>

            </div>

            <div class="form-group row">

                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Isi informasi</label>

                <div class="col-sm-9">

                    <textarea class="form-control" name="konten">{{ $informasi->konten }}</textarea>

                </div>

            </div>

            <div class="form-group row">

                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Peta</label>

                <div class="col-sm-9">

                    <input type="text" class="form-control" id="lname" value="{{ $informasi->peta }}" placeholder="Masukkan Link" name="peta">

                </div>

            </div>

            <div class="form-group row">

                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Foto 1</label>

                <div class="col-sm-9">

                    <input type="file" name="foto1">

                </div>

            </div>

            <div class="form-group row">

                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Foto 2</label>

                <div class="col-sm-9">

                    <input type="file" name="foto2">

                </div>

            </div>

            <div class="form-group row">

                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Foto 3</label>

                <div class="col-sm-9">

                    <input type="file" name="foto3">

                </div>

            </div>

        </div>

        <div class="border-top">

            <div class="card-body">

                <button type="submit" class="btn btn-primary">Ubah</button>

            </div>

        </div>

    </form>

</div>

@endsection