@extends('admin.master')
@section('content')
<div class="card">
<form action="{{route('storeInformasi')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
{{csrf_field()}}
        <div class="card-body">
            <h4 class="card-title">Input Informasi Wisata</h4>
            <div class="form-group row">
                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Wisata</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="fname" placeholder="Nama Wisata" name="judul">
                </div>
            </div>
            <div class="form-group row">
                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Kategori</label>
                <div class="col-sm-9">
                    <select type="text" class="form-control" id="fname" name="id_kategori">
                        <option disabled selected>Pilih Kategori</option>
                        @foreach ($kategoris as $kategori)
                            <option value="{{$kategori->id}}">{{$kategori->nama_kat}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="lname" placeholder="Nama Jalan" name="jalan">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label"></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="lname" placeholder="Kelurahan/Kecamatan" name="kelkec">
                </div>
            </div>
            <div class="form-group row">
                <label for="email1" class="col-sm-3 text-right control-label col-form-label"></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="email1" placeholder="Kota/Kabupaten" name="kotakab">
                </div>
            </div>
            <div class="form-group row">
                <label for="cono1" class="col-sm-3 text-right control-label col-form-label"></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="cono1" placeholder="Provinsi" name="provinsi" value="Sumatera Barat" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Isi informasi</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="konten"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Peta</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="lname" placeholder="Masukkan Link" name="peta">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Foto 1</label>
                <div class="col-sm-9">
                    <input type="file" name="foto1">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Foto 2</label>
                <div class="col-sm-9">
                    <input type="file" name="foto2">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Foto 3</label>
                <div class="col-sm-9">
                    <input type="file" name="foto3">
                </div>
            </div>
        </div>
        <div class="border-top">
            <div class="card-body">
                <button type="submit" class="btn btn-primary">Input</button>
            </div>
        </div>
    </form>
</div>
@endsection