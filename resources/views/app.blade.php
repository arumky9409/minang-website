<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from tf.wpcheatsheet.net/html/travel/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 May 2019 13:07:11 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('backend')}}/assets/images/iconwm.png">
        <title>{{$title}}</title>
        
        <!--bootstrap.min.css -->
        <link href="{{asset('frontend')}}/assets/css/bootstrap.min.css" rel="stylesheet">
        <!--font-awesome.min.css -->
        <link href="{{asset('frontend')}}/assets/css/font-awesome.min.css" rel="stylesheet">
        <!--material-design-iconic-font.min.css -->
        <link href="{{asset('frontend')}}/assets/css/material-design-iconic-font.min.css" rel="stylesheet">
        <!--owl.carousel.min.css -->
        <link href="{{asset('frontend')}}/assets/css/owl.carousel.min.css" rel="stylesheet">
        <!--magnific-popup.css -->
        <link href="{{asset('frontend')}}/assets/css/magnific-popup.css" rel="stylesheet">
        <!--nice-select.css -->
        <link href="{{asset('frontend')}}/assets/css/nice-select.css" rel="stylesheet">
        <!--slicknav.min.css -->
        <link href="{{asset('frontend')}}/assets/css/slicknav.min.css" rel="stylesheet">
        <!--style.css -->
        <link href="{{asset('frontend')}}/assets/css/style.css" rel="stylesheet">
        <!--responsive.css -->
        <link href="{{asset('frontend')}}/assets/css/responsive.css" rel="stylesheet">
    </head>
    <body>
        <!--Popup search area start -->
        <div id="popup-search-box-id" class="popup-search-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="popup-search-box-inner">
                            <form action="http://tf.wpcheatsheet.net/html/travel/mail.php">
                                <div class="row">
                                    <div class="col-md-10">
                                        <input type="search" placeholder="Search here...">
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit"><i class="zmdi zmdi-search"></i></button>
                                    </div>
                                </div>
                            </form>
                            <span class="close-btn-search"><i class="zmdi zmdi-close"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Popup search area end --> 
        <!--page-welcome-area start --> 
        <div class="page-welcome-area">
            <div class="header-area header-absolute page-header-style">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 text-center">
                            <div class="logo">
                                <a href="{{url('/')}}"><img src="{{asset('frontend/assets') }}/img/logo1.png" alt=""></a> 
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="mainmenu">
                                <ul id="slicknav-menu-list">                                
                                    <li><a href="{{url('/')}}">Beranda</a></li>
                                    <li><a href="{{route('indexAlam')}}">Wisata Alam</a></li>
                                    <li><a href="{{route('indexKuliner')}}">Wisata Kuliner</a></li>
                                    <li><a href="{{route('indexEdu')}}">Wisata Edukasi</a></li>
                                    <li><a href="{{route('indexBudaya')}}">Wisata Budaya</a></li>
                                    <li><a href="{{route('showPesan')}}">Kontak</a></li>
                                </ul> 
                            </div>
                            <div class="slicknav-menu-wrap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


            @yield('content')

            
        <!--footer-area start -->
        <div class="footer-area hp2-footer-area">
            <div class="footer-top-area padding-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="footer-widget pink-icon-widget">
                                <div class="footer-logo-hp2">
                                    <img src="{{asset('frontend')}}/assets/img/logofoot.png" alt="">
                                </div>
                                <ul>
                                    <li><a href="#">Sumatera Barat, Indonesia </a></li>
                                    <li><a href="mailto:arumky.shalima@gmail.com">arumky.shalima@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>

                        @php
                            $terbarus = App\Informasi::all();
                        @endphp

                        <div class="col-md-4 col-sm-6">
                            <div class="footer-widget list-widget">
                                <h4>Informasi Terbaru</h4>
                                <ul>
                                    @foreach ($terbarus->sortByDesc('updated_at')->slice(0,5) as $terbaru)
                                        <li><a href="{{url('detail/'.$terbaru->id)}}">{{$terbaru->judul}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-5 col-sm-6">
                            <div class="footer-widget destination-widget">
                                <h4>Bagaimana Penilaian Anda Terhadap Web Ini?</h4>
                                <form action="{{url('/welcome/{succes}')}}" method="POST">
                            {{csrf_field()}}
                            <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" name="id" value="1">
                                        <input name="polling" value="Sangat Baik" type="radio">
                                        <label for="contact-us-name">Sangat Baik</label><br>
                                        <input name="polling" value="Baik" type="radio">
                                        <label for="contact-us-name">Baik</label><br>
                                        <input name="polling" value="Cukup" type="radio">
                                        <label for="contact-us-name">Cukup</label><br>
                                        <input name="polling" value="Kurang" type="radio">
                                        <label for="contact-us-name">Kurang</label><br>
                                    </div>
                                </div>
                                <button type="submit" class="pink-btn">Nilai</button>
                                <!-- <button type="submit" class="yellow-btn text-black">Nilai</button> -->
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            &copy; 2019 Wisata Minang <br>
                        Designed by Arsha with <i class="zmdi zmdi-favorite-outline"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer-area end -->

        <!--jquery.min.js-->
        <script src="{{asset('frontend')}}/assets/js/jquery.min.js"></script>
        <!--bootstrap.min.js-->
        <script src="{{asset('frontend')}}/assets/js/bootstrap.min.js"></script>
        <!--owl.carousel.min.js-->
        <script src="{{asset('frontend')}}/assets/js/owl.carousel.min.js"></script>
        <!--magnific-popup-1.1.0.js-->
        <script src="{{asset('frontend')}}/assets/js/magnific-popup-1.1.0.js"></script>
        <!--jquery.nice-select.min.js-->
        <script src="{{asset('frontend')}}/assets/js/jquery.nice-select.min.js"></script>
        <!--jquery.waypoints.4.0.0.min.js-->
        <script src="{{asset('frontend')}}/assets/js/jquery.waypoints.4.0.0.min.js"></script>
        <!--jquery.counterup.min.js-->
        <script src="{{asset('frontend')}}/assets/js/jquery.counterup.min.js"></script>
        <!--jquery.slicknav.min.js-->
        <script src="{{asset('frontend')}}/assets/js/jquery.slicknav.min.js"></script>
        <!--main.js-->
        <script src="{{asset('frontend')}}/assets/js/main.js"></script>
    </body>

<!-- Mirrored from tf.wpcheatsheet.net/html/travel/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 May 2019 13:07:15 GMT -->
</html>