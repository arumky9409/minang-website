@extends('app')
@section('content')
            <div class="slider-area text-center owl-carousel hp2-slider-area">
                <div class="single-slide-item single-slide-item-bg1" data-dot="1">
                    <div class="single-slide-item-table-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <h1>Selamat Datang<br>di Website Kami</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-slide-item single-slide-item-bg2" data-dot="2">
                    <div class="single-slide-item-table-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <h1>Festival Tabuik</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-slide-item single-slide-item-bg3" data-dot="3">
                    <div class="single-slide-item-table-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <h1>Jam Gadang</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--welcome-area end -->
        <!--hompage-2-contents-area start -->
        <div class="padding-top hompage-2-contents-area">
            <div class="world-out-there-area hp2-area-bg hp2-area-bg-right section-padding-hp2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="world-out-there-left">
                                <h1>Sumatera Barat</h1>
                                <p>Provinsi Sumatera Barat merupakan salah satu provinsi di Indonesia yang terletak di sebelah barat Pulau Sumatera dengan Padang sebagai ibukotanya. Provinsi ini berbatasan langsung dengan 4 provinsi, yakni Sumatera Utara, Riau, Jambi dan Bengkulu.</p>
                                <p>Sebagian penduduk Sumatera Barat beretnis Minangkabau dengan mayoritas penduduknya memeluk agama Islam. Sumatera Barat terdiri dari 12 kabupaten dan 7 kota dengan pembagian wilayah administratif sesudah kecamatan di seluruh kabupaten yang dinamakan sebagai <i>nagari</i>.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="world-out-there-right">
                                <a href="https://www.youtube.com/watch?v=UjYPt-slIQk" class="mfp-iframe world-out-video-btn">Klik untuk lihat video <i class="zmdi zmdi-play-circle-outline"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-title-hp2 text-center section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <h1>Destinasi Populer</h1>
                        </div>
                    </div>
                </div>
            </div>
            @php
                $populars = App\Informasi::where('status', 'Populer')->get();
            @endphp
            <div class="destinations-honeymoon-area text-center hp2-area-bg section-padding-hp2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="destination-honeymoon-carousel owl-carousel">
                                @foreach ($populars as $populer)
                                <div class="destinations-honeymoon-single-item">
                                    <div class="destination-honeymoon-bg" style="background-image: url({{asset('images/'.$populer->foto1)}})">
                                    </div>
                                    <div class="destination-honeymoon-text">
                                        <h4>{{$populer->kotakab}}</h4>
                                        <h3><a href="{{url('detail/'.$populer->id)}}">{{$populer->judul}}</a></h3>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- <div class="section-title-hp2 text-center section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <h1>best holiday</h1>
                            <h1>packages</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="holiday-packages-area hp2-area-bg hp2-area-bg-right section-padding-hp2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="holiday-packages-left-item padding-top">
                                <p>Seconds because when I came to again I was still laughing at Jonah Timothy Simons the act or who plays Jonah responded on Twitter oh my god oh my god he promised .</p>
                            <a href="#" class="pink-btn">More packages</a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="holiday-packages-right-carousel owl-carousel text-center">
                                <div class="holiday-packages-single-item">
                                    <div class="holiday-packages-item-bg holiday-packages-item-bg1"></div>
                                    <div class="holiday-packages-item-text">
                                        <h4>GRAND SWITZERLAND</h4>
                                        <p>Shopping, Mountain, Snow</p>
                                        <h2>$2500</h2>
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-case-play"></i>6 Days 5 Night</a></li>
                                            <li><a href="#"><i class="zmdi zmdi-case-play"></i>4 People</a></li>
                                        </ul>
                                        <a href="tours.html" class="pink-btn">View details</a>
                                    </div>
                                </div>
                                <div class="holiday-packages-single-item">
                                    <div class="holiday-packages-item-bg holiday-packages-item-bg2"></div>
                                    <div class="holiday-packages-item-text">
                                        <h4>san francisco, usa</h4>
                                        <p>Las vegas, Nevada</p>
                                        <h2>$2800</h2>
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-case-play"></i>4 Days 3 Night</a></li>
                                            <li><a href="#"><i class="zmdi zmdi-case-play"></i>4 People</a></li>
                                        </ul>
                                        <a href="tours.html" class="pink-btn">View details</a>
                                    </div>
                                </div>
                                <div class="holiday-packages-single-item">
                                    <div class="holiday-packages-item-bg holiday-packages-item-bg1"></div>
                                    <div class="holiday-packages-item-text">
                                        <h4>GRAND SWITZERLAND</h4>
                                        <p>Shopping, Mountain, Snow</p>
                                        <h2>$2500</h2>
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-case-play"></i>6 Days 5 Night</a></li>
                                            <li><a href="#"><i class="zmdi zmdi-case-play"></i>4 People</a></li>
                                        </ul>
                                        <a href="tours.html" class="pink-btn">View details</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            
            <div class="section-title-hp2 testi-section-title-hp2 text-center padding-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <h1>Apa Kata Mereka?</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            @php
            $pesan = App\Pesan::All();
            $pesans = App\Pesan::where('validation', 1)->get();
            @endphp
            
            <div class="hp2-testimonial-area section-padding ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="hp2-testimonial-carousel owl-carousel text-center">
                                @foreach ($pesans as $pesan)
                                <div class="hp2-testimonial-carousel-single">
                                    <h4>{{$pesan->nama}}</h4>
                                    <p><i>"{{$pesan->pesan}}"</i></p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--hompage-2-contents-area end -->


@endsection