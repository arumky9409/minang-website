-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2021 at 12:25 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minang`
--

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `judul` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kategori` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jalan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelkec` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kotakab` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `konten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `peta` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto3` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id`, `judul`, `id_kategori`, `jalan`, `kelkec`, `kotakab`, `provinsi`, `konten`, `peta`, `status`, `foto1`, `foto2`, `foto3`, `created_at`, `updated_at`) VALUES
(6, 'Istana Pagaruyung', '4', 'Jalan Sutan Alam Bagagarsyah, Pagaruyung', 'Kec. Tanjung Emas', 'Kab. Tanah Datar', 'Sumatera Barat', 'Istana Pagaruyung atau Istano Baso ini terletak di kota Batusangkar, berjarak lebih kurang 5 km dari pusat kota Batusangkar,  Istana ini merupakan peninggalan sejarah yang dulunya berfungsi sebagai kediaman Raja Minangkabau. Istana Pagaruyung yang sekarang adalah replika dari yang asli karena istana yang asli, yang terletak di atas Bukit Batu Patah, pernah terbakar karena kerusuhan pada tahun 1804 pada saat Perang Paderi. Tiket masuk Istana Paguruyung adalah Rp15.000 untuk wisatawan lokal dan Rp25.000 untuk wisatawan mancanegara. Di sini Anda juga dapat mencoba pakaian adat Minangkabau dengan biaya sewa Rp30.000 untuk dewasa dan Rp20.000 untuk anak-anak. Di istana ini juga terdapat lebih dari 100 replika furnitur dan artefak antik Minang dengan tujuan menghidupkan istana kembali sebagai pusat budaya Minangkabau serta objek wisata Sumatera Barat.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.6833421360593!2d100.61921621475342!3d-0.47129639965615117!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e2ad2d2be29c7dd%3A0xb19e3eb230efffbc!2sIstano+Basa+Pagaruyung!5e0!3m2!1sen!2sid!4v1564481600931!5m2!1sen!2sid', 'Populer', 'pagaruyung2.jpg', 'pagaruyung3.jpg', 'pagaruyung1.jpg', '2019-06-25 08:32:04', '2019-07-30 05:51:23'),
(7, 'Sate Padang Mak Syukur', '2', 'Jalan Mohammad Syafei No.63, Kelurahan Pasar Baru', 'Kecamatan Padang Panjang Barat', 'Kota Padang Panjang', 'Sumatera Barat', 'Berwisata ke Sumatera Barat tidak lengkap kalau belum mencoba kuliner satu ini. Sate Mak Syukur merupakan restoran sate padang legendaris yang berlokasi di Kota Padang Panjang yang sudah mulai beroperasi mulai jam 09.30-21.00. Restoran sate padang ini senantiasa ramai setiap harinya dan jika sudah hari libur, jangan terlalu berharap akan langsung mendapat tempat duduk. Tidak seperti sate padang pada umumnya, sate mak syukur memiliki kuah sate yang cenderung lebih kuning. Selain di Jl. Mohammad Syafei, Sate Mak Syukur dapat ditemui di Jl. Sutan Syahrir No.250, Silaing Bawah, Padang Panjang.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.6874480495107!2d100.39840691399566!3d-0.46407279966141035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd5251096b41531%3A0xb113d4165cbeb05b!2sSate+Mak+Syukur!5e0!3m2!1sen!2sid!4v1562300584250!5m2!1sen!2sid', 'Populer', 'sate1.jpg', 'sate2.jpg', 'sate3.jpg', '2019-07-20 03:04:20', '2019-07-25 00:39:24'),
(9, 'Rumah Puisi Taufik Ismail', '3', 'Jalan Raya Padang Panjang - Bukittinggi Km.6', 'Kec. Sepuluh Koto', 'Kab. Tanah Datar', 'Sumatera Barat', 'Rumah Puisi ini dibangun oleh Taufik Ismail dan  Ati Ismail pada tahun 2008.  Tujuan didirikannya Rumah Puisi adalah sebagai tempat pelatihan untuk guru dan siswa. Pelatihan yang diberikan berupa menulis, membaca dan apresisasi sastra. Tujuan lainnya adalah sebagai perpustakaan yang saat ini memiliki koleksi 8000-an judul buku.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.7144830748!2d100.40408891475337!3d-0.4133696996984094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd53af077323d49%3A0x2c57be2ffd12a480!2sPoetry+House+Taufiq+Ismail!5e0!3m2!1sen!2sid!4v1561976863781!5m2!1sen!2sid', NULL, 'rumah_puisi2.jpg', 'rumah_puisi3.jpg', 'rumah_puisi1.jpg', '2019-07-20 03:10:30', '2019-07-30 05:54:30'),
(10, 'Museum Bung Hatta', '3', 'Jalan Soekarno-Hatta No.37', 'Mandiangin Koto Selatan', 'Kota Bukittinggi', 'Sumatera Barat', 'Museum  ini merupakan tempat Bung Hatta dilahirkan dan dibesarkan sampai berusia 11 tahun. Rumah yang didirikan sekitar tahun 1980-an ini terdiri dari bangunan utama, pavillion, lumbung padi, dapur, serta kandang kuda. Bangunan utam berfungsi sebagai ruang menerima tamu, ruang makan keluarga dan kamar tidur. Museum ini dibuka dari jam 8 pagi.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.7633450502094!2d100.3709237147533!3d-0.30077749978055013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd54780bbc3b44b%3A0xc6fe4d827fb903a2!2sBung+Hatta+Birthplace+Museum!5e0!3m2!1sen!2sid!4v1561978885204!5m2!1sen!2sid', NULL, 'bung_hatta1.jpg', 'bung_hatta2.jpg', 'bung_hatta3.jpg', '2019-07-20 03:14:26', '2019-07-20 03:14:26'),
(11, 'Lubang Jepang', '3', 'Jalan Panorama, Bukit Cangang Kayu Ramang', 'Kecamatan Guguk Panjang', 'Kota Bukittinggi', 'Sumatera Barat', 'Lubang Jepang atau dapat disebut juga dengan Goa Jepang berada dalam kawasan objek wisata Taman Panorama Bukittinggi. Lubang Jepang merupakan peninggalan tentara Jepang yang digunakan sebagai tempat perlindungan sekitar tahun 1942-1945. Lubang ini memiliki panjang sekitar 1,5 kilometer, akan tetapi demi keamanan yang dibuka untuk umum hanya sepanjang 750 meter saja.', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15959.043116554682!2d100.366119!3d-0.3077167!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x57754f9fe912dcab!2sJapanese+Tunnel+Bukittinggi!5e0!3m2!1sen!2sid!4v1563263360896!5m2!1sen!2sid', NULL, 'lobang-jepang1.jpg', 'lobang-jepang2.jpg', 'lobang-jepang3.jpg', '2019-07-20 03:16:39', '2019-07-30 05:56:07'),
(12, 'Air Terjun Nyarai', '1', 'Jorong Gamaran, Korong Salibutan', 'Kec. Lubuk Alung', 'Kab. Padang Pariaman', 'Sumatera Barat', 'Air Terjun Nyarai ini berada di pedalaman Hutan Gamaran. Dengan menggunakan mobil, dari Bandara Internasional Minangkabau membutuhkan waktu sekitar 1.5 jam, sedangkan bila dari Kota Padang membutuhkan waktu sekitar 30 menit untuk mencapai posko utama Hutan Gamaran. Kemudian dari posko utama tersebut, dilanjutkan lagi dengan berjalan kaki sepanjang 5,3 kilometer.  Rasa lelah selama diperjalanan akan segera terbayarkan ketika sampai di tujuan.  Air yang masih jernih dan berwarna kehijauan, diapit oleh dua batu besar, serta dikelilingi tanaman hijau yang masih asri. Untuk masuk ke kawasan wisata ini, pengunjung hanya cukup membayar Rp20.000 untuk tracking atau membayar sebesar Rp40.000 jika ingin melakukan camping.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.534419605667!2d100.36018791475341!3d-0.6835131995013409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd4dbed186bfa37%3A0x4a825bbc1a6b4303!2sAir+Terjun+Nyarai!5e0!3m2!1sen!2sid!4v1563640771305!5m2!1sen!2sid', NULL, 'nyarai2.jpg', 'nyarai3.jpg', 'nyarai1.jpg', '2019-07-20 09:42:01', '2019-07-30 05:56:29'),
(13, 'Ngarai Sianok', '1', 'Jl. Panorama, Bukit Cangang Kayu Ramang', 'Kecamatan Guguk Panjang', 'Kabupaten Agam', 'Sumatera Barat', 'Ngarai Sianok merupakah lembah curam yang berada di Kota Bukittinggi. Lembah ini memiliki kedalaman 100 meter dengan panjangnya yang membentang dari selatan Nagari Koto Gadang sampai ke utara Nagari Sianok Enam Suku sepanjang 15 kilometer, serta lebarnya 200 meter. Di dasar Ngarai Sianok ini terdapat sebuah anak sungai yang airnya jernih dan hidup berbagai macam tumbuhan dan hewan langka.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.7605861493653!2d100.36202321475332!3d-0.30823229977511096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd538bc116a0d43%3A0x2f790b71cab9c6c9!2sSianok+Valley!5e0!3m2!1sen!2sid!4v1563641530817!5m2!1sen!2sid', NULL, 'ngarai_sianok1.jpg', 'ngarai_sianok2.jpg', 'ngarai_sianok3.jpg', '2019-07-20 10:34:44', '2019-07-20 10:34:44'),
(14, 'Jam Gadang', '3', 'Benteng Pasar Atas', 'Kecamatan Guguk Panjang', 'Kota Bukittinggi', 'Sumatera Barat', 'Jam Gadang (dalam bahasa Indonesia berarti Jam Besar) adalah sebuah menara jam yang berada di pusat Kota Bukittinggi dengan tinggi sekitar 26 meter yang penyangganya dibuat dari campuran kapur, putih telur dan pasir putih. Jam Gadang ini merupakan hadiah dari Ratu Belanda untuk Rook Maker, seorang Controleur Sekretaris Kota Bukittinggi pada masa Pemerintahaan Belanda saat itu, yang langsung didatangkan dari Rotterdam, Belanda. Disetiap sisinya terdapat jam dengan masing-masing berdiameter 80 centimeter. Keunikan dari jam ini adalah pada penulisan angka 4 dipenunjuknya, yaitu yang seharusnya angka romawi untuk angka 4 adalah \'IV\'  tetapi pada Jam Gadang ditulis \'IIII\'. Hal unik lainnya adalah jam ini bergerak secara mekanik oleh mesin yang hanya dibuat dua unit di dunia, yaitu Jam Gadang dan Big Ben di London, Inggris. Jam Gadang memiliki atap bergonjong yang merupakan atap dari rumah adat Minangkabau (rumah gadang).', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.761731803436!2d100.3673314647533!3d-0.30515874977735213!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd538bce0251993%3A0x671c755cea2a2d4!2sJam+Gadang%2C+Benteng+Pasar+Atas%2C+Guguk+Panjang%2C+Bukittinggi+City%2C+West+Sumatra+26136!5e0!3m2!1sen!2sid!4v1563647768579!5m2!1sen!2sid', 'Populer', 'jamgadang1.jpg', 'jamgadang2.jpg', 'jam.jpg', '2019-07-20 11:37:23', '2019-07-30 05:57:35'),
(15, 'Danau Singkarak', '1', NULL, 'Kec. X Koto Singkarak', 'Kab. Tanah Datar & Kab. Solok', 'Sumatera Barat', 'Danau Singkarak merupakan danau terluas kedua di Pulau Sumatera setelah Danau Toba yang memiliki luas sekitar 107 kilometer persegi dengan kedalaman sekitar 268 meter. Danau ini termasuk jenis danau tektonik yang terbentuk karena aktivitas lempeng bumi. Di Danau Singkarak hidup berbagai jenis ikan, salah satunya adalah ikan bilih. Ikan bilih sulit dibudidayakan di luar danau ini, kalaupun bisa akan memiliki cita rasa yang berbeda. Danau Singkarak juga merupakan salah satu jalur lintas untuk acara Tour de Singkarak.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d127666.78391085676!2d100.47077723611004!3d-0.616982289039168!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd4d5e42603f255%3A0x6d533fe568da06d2!2sLake+Singkarak!5e0!3m2!1sen!2sid!4v1564045968627!5m2!1sen!2sid', 'Populer', 'singkarak1.jpg', 'singkarak2.jpg', 'singkarak3.jpg', '2019-07-25 02:29:46', '2019-07-30 05:57:58'),
(16, 'Pantai Air Manis', '1', NULL, 'Kelurahan Air Mani', 'Kec. Padang Selatan', 'Sumatera Barat', 'Di Pantai Air Manis pengunjung dapat menikmati panorama dari pantai ini, selain itu juga dapat melihat langsung Batu Malin Kundang. Batu ini sangat menyerupai manusia yang sedang bersujud dan disekitarnya dikelilingi batu yang mirip dengan pecahan kapal. Malin Kundang sendiri merupakan legenda yang sangat terkenal dari Sumatera Barat yang menceritakan tentang seorang pemuda yang durhaka terhadap ibunya sehingga dikutuk menjadi batu. Untuk menuju lokasi wisata ini, hanya dibutuhkan waktu sekitar 20-30 menit dengan menggunakan kendaraan bermotor dari Kota Padang', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.220770089328!2d100.3609933!3d-0.991639249999989!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd4bb8fd5268b13%3A0xa991ccf7184d219b!2sAir+Manis+Beach!5e0!3m2!1sen!2sid!4v1564047762615!5m2!1sen!2sid', NULL, 'pantai_airmanis3.jpg', 'pantai_airmanis1.jpg', 'pantai_airmanis2.jpg', '2019-07-25 03:37:15', '2019-07-30 05:58:28'),
(17, 'Lembah Harau', '1', 'Nagari Harau', 'Kec. Harau', 'Kab. Lima Puluh Koto, Kota Payakumbuh', 'Sumatera Barat', 'Lembah Harau memiliki luas wilayah sekitar 270 hektar. Wisata ini sudah ditetapkan sebagai cagar alam dan suaka margasatwa sejak tanggal 10 Januari 1993. Salah satu hewan khas yang hidup di wilayah ini adalah monyet ekor panjang, yang termasuk spesies hewan langka Sumatera. Wisata ini menyediakan beberapa pondok kecil untuk menginap dengan harga sewa dimulai dari Rp50.000 per malamnya. Disini juga tersedia fasilitas kolam pemandian, tempat berkemah dan jalan setapak untuk hiking keliling kawasan.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.812238641438!2d100.6659272!3d-0.10003999999999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e2aac5605cb2127%3A0x7789df97bf998e2e!2sHarau+Valley+Waterfall!5e0!3m2!1sen!2sid!4v1564051426316!5m2!1sen!2sid', NULL, 'harau1.jpg', 'harau2.jpg', 'harau3.jpg', '2019-07-25 04:18:49', '2019-07-25 04:18:49'),
(18, 'Lembah Anai', '1', 'Jalan Padang-Bukittinggi, Nagari Singgalang', 'Kec. Sepuluh Koto', 'Kab. Tanah Datar', 'Sumatera Barat', 'Air Terjun Lembah Anai ini merupakan salah satu wisata favorit yang dikunjungi oleh wisatawan. Berada di sebelah barat kawasan Cagar Alam Lembah Anai kaki Gunung Singgalang, jika melewati lintas Padang-Bukittinggi akan langsung terlihat karena letaknya yang berada tepat si pinggir jalan. Lembah Anai memiliki ketinggian kurang lebih 35 kilometer dengan kondisi air yang jernih dan dingin. Wisatawan yang ingin berkunjung ke lokasi wisata ini, hanya dikenakan biaya sebesar Rp1.500 per orangnya. Fasilitas yang ada disekitar wisata ini, antara lain gazebo, warung atau rumah makan, serta kamar mandi umum.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.6761901649975!2d100.33621531399572!3d-0.4836213996471576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd5239b04efe7d3%3A0xfa4a00004d31f7ec!2sLembah+Anai+Waterfall!5e0!3m2!1sen!2sid!4v1564304883130!5m2!1sen!2sid', 'Populer', 'anai1.jpg', 'anai2.jpg', 'anai3.jpg', '2019-07-28 02:56:48', '2019-07-30 05:59:20'),
(19, 'Kedai Kopi Nan Yo', '2', 'Jalan Niaga Nomor 205, Kawasan Pondok', 'Kec. Padang Barat', 'Kota Padang', 'Sumatera Barat', 'Kedai Kopi Nan Yo merupakan kedai kopi tertua yang ada di Kota Padang. Didirikan pada tahun 1932 oleh Than Tek Tjiaw yang saat ini sudah dipegang oleh generasi ketiga, yaitu Victor Bostani. Walaupun sudah dipegang oleh generasi ketiga, kopi nan yo tetap memiliki cita rasa yang sama sehingga masih terasa autentik. Kedai Kopi Nan Yo menggunakan biji kopi asli Sumatra Barat, yaitu biji kopi Sidakalang, yang disajikan dengan menggunakan metode ala Hainam. Pertama, bubuk kopi dimasukkan ke dalam saringan panjang berbahan kain, lalu dimasukkan ke dalam teko alumunium bertangkai panjang, kemudian kopi disiram dengan menggunakan air panas. Penyajian dengan metode ini yang membuat kopi Nan Yo tidak memiliki ampas. Di kedai ini, disetiap mejanya disediakan camilan seperti bakwan, kacang tanah gireng, kue sarikaya dan lainnya. Harga kopi yang dijual di sini, secangkirnya dimulai dari Rp10.000.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.2610293688062!2d100.35937366399601!3d-0.9576506993014071!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd4b95a8af02c69%3A0x235621b44427e149!2sWarung+Kopi+Nan+Yo!5e0!3m2!1sen!2sid!4v1564310516424!5m2!1sen!2sid', NULL, 'kopinanyo1.jpg', 'kopinanyo2.jpg', 'kopinanyo3.jpg', '2019-07-28 03:53:02', '2019-07-28 05:46:46'),
(20, 'Nasi Kapau Uni Lis', '2', 'Jalan Pemuda Los Lambuang', 'Kec. Guguk Panjang', 'Kota Bukittinggi', 'Sumatera Barat', 'Nasi Kapau Uni Lis merupakan rumah makan nasi kapau legendaris yang disajikan oleh Uni Lis sejak tahun 1970. Kapau sendiri merupakan nama kecamatan yang ada di Kabupaten Agam sekitar 40 menit dari Bukittinggi. Nasi kapau merupakan makanan khas Kota Bukittinggi berupa nasi pulen yang disajikan dengan berbagai lauk pauk khas. Nasi kapau berbeda dengan nasi padang yang dijual pada umumnya. Bedanya ada pada proses memasak nasinya, di mana nasi kapau dimasak lebih lama menggunakan kayu bakar dengan api yang besar, kemudian dilanjutkan dengan bara. Harga per porsinya dimulai dari Rp10.000 tergantung lauk yang dipilih. Rumah makan ini dibuka mulai jam 09.00 sampai 18.00 WIB.', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d997.4405437878182!2d100.371309!3d-0.303961!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4b0d2250519e4206!2sNasi+Kapau+UNi+Lis!5e0!3m2!1sen!2sus!4v1564312419134!5m2!1sen!2sus', NULL, 'kapau3.jpg', 'kapau2.jpg', 'kapau4.jpg', '2019-07-28 05:44:15', '2019-07-30 05:53:59'),
(21, 'Museum Adityawarman', '3', 'Jalan Diponegoro No.10', 'Kec. Padang Barat', 'Kota Padang', 'Sumatera Barat', 'Museum Adityawarman dibangun pada tahun 1974 dan diresmikan pada tahun 1977. Nama dari museum ini sendiri diambil dari nama seorang raja Malaypura pada abad ke-14. Arsitektur dari bangunan ini terinspirasi dari rumah gadang yang merupakan rumah adat dari suku Minangkabau. Museum Adityawarman merupakan museum yang penting karena mengangkat sejarah masyarakat Minangkabau, serta peninggalan Minangkabau mulai dari tradisional sampai modern. Selain melihat koleksi-koleksi yang ada di museum ini, wisatawan juga dapat mengetahui sistem adat Minang yang dipresentasikan pada suatu diorama yang ada di ruang tengah. Sistem tersebut adalah Sistem Matrilineal di mana perempuan memegang peran yang sangat kuat. Peran-peran tersebut dijelaskan secara lengkap di museum ini. Untuk ke tempat ini, wisatawan hanya membayar tiket masuk Rp2.000 untuk dewasa dan Rp1.000 untuk anak-anak. Museum Adityawarman beroperasi pada jam  08.00-18.00 WIB.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.26379657236!2d100.35363961475363!3d-0.9552700993031474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd4b94e77661571%3A0x81991e01942fc77b!2sMuseum+Adityawarman!5e0!3m2!1sen!2sid!4v1564320925809!5m2!1sen!2sid', NULL, 'adityawarman1.jpg', 'adityawarman2.jpg', 'adityawarman3.jpg', '2019-07-28 07:00:56', '2019-07-28 07:00:56'),
(22, 'Museum Kereta Api Sawahlunto', '3', 'Jalan Kampung Teleng', 'Kelurahan Pasar, Kecamatan Lembah Segar', 'Kota Sawahlunto', 'Sumatera Barat', 'Museum Kereta Api Sawahlunto ini merupakan museum kereta kedua yang ada di Indonesia, setelah Museum Kereta Api Ambarawa yang ada di Jawa Tengah. Museum ini buka pada hari Selasa s/d Minggu jam 08.00-17.00, sedangkan hari Senin tutup untuk keperluan perawatan. Harga tiket masuk museum ini adalah Rp3.000 untuk dewasa dan untuk pelajar/anak Rp2.000. Sebelum menjadi museum pada tahun 2005, tempat ini merupakan stasiun kereta api untuk kepentingan pengangkutan batu bara. Tetapi saat ini digunakan untuk melayani kereta api wisata setiap hari Minggu. Museum ini memiliki 106 buah koleksi di mana salah satu koleksi terkenalnya adalah Lokomotif Uap bergigi E1060 atau yang dikenal dengan istilah \"Mak Itam\".', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.534870662872!2d100.77475191475352!3d-0.6829699995017452!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e2b2f3ff366abb7%3A0x6d1618f86e45daec!2sMuseum+Stasiun+Kereta+Api+Sawahlunto!5e0!3m2!1sen!2sid!4v1564332692968!5m2!1sen!2sid', NULL, 'kereta2.jpg', 'kereta1.jpg', 'kereta3.jpg', '2019-07-28 10:42:03', '2019-07-30 06:00:30'),
(23, 'Kain Songket Pandai Sikek', '4', 'Nagari Pandai Sikek', 'Kec. Sepuluh Koto', 'Kab. Tanah Datar', 'Sumatera Barat', 'Nagari Pandai Sikek dikenal dengan kain songket tradisionalnya yang memiliki kekhasan motif sejak masa lampau. Kekhasan ini sudah diwariskan secara turun-temurun sehingga tetap lestari sampai saat ini. Motif-motif tersebut antara lain motif batang pinang (pohon pinang), motif bijo bayam (biji bayam) dan motif saluak laka. Mayoritas perempuan di Pandai Sikek pandai menenun, yang kebanyakan sudah mulai belajar menenun sejak usia 12-15 tahun. Karena tradisi tenun songket yang kuat hingga saat ini, eksistensi dari tenun songket diapresiasikan oleh Pemerintah Republik Indonesia dalam gambar mata uang pecahan Rp5.000 emisi 1999-saat ini.\r\n\r\nNagari Pandai Sikek sendiri terletak di antara Gunung Singgalang dan Gunung Marapi. Di Nagari ini ada berbagai toko yang menjual hasil tenun songketnya, salah satunya adalah Toko Satu Karya. Harga ain tenun songket yang dijual di sini bervariasi, tergantung lama proses pengerjaannya, tingkat kesulitan motif dan bahan yang digunakan. Di dalam toko ini ada sebuah alat tenun tradisional di mana pengunjung dapat mencobanya.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d90277.01824070004!2d100.32128480165056!3d-0.4058299173857153!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd53b99eed605d1%3A0xfa3df1e50dfb1943!2sPandai+Sikek%2C+Sepuluh+Koto%2C+Tanah+Datar+Regency%2C+West+Sumatra!5e0!3m2!1sen!2sid!4v1564473746277!5m2!1sen!2sid', 'Populer', 'pandai-sikek3.jpg', 'pandai-sikek1.jpg', 'pandai-sikek2.jpg', '2019-07-30 01:16:32', '2019-07-30 02:20:49'),
(24, 'Festival Tabuik', '4', NULL, 'Pantai Gandoriah', 'Kota Pariaman', 'Sumatera Barat', 'Festival Tabuik adalah upacara adat yang dilaksanakan setiap tanggal 10 Muharram oleh masyarakat Kota Pariaman untuk memperingati wafatnya Hussain, cucu dari Nabi Muhammad SAW. Tabuik sendiri merupakan istilah untuk usungan jenazah yang dibawa selama prosesi acara tersebut. Pada mulanya festival ini merupakan upacara untuk penganut Syi\'ah, akan tetapi mayoritas masyarakat yang melakukan upacara ini adalah penganut Sunni. Festival ini sendiri sudah berlangsung sejak abad ke-19 Masehi yang diperkenalkan oleh Pasukan Tamil Muslim Syi\'ah dari India. Festival Tabuik tidak hanya menjadi adat masyarakat setempat, tetapi sudah menjadi salah satu bagian dari komoditi pariwisata daerah.', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.58068640426!2d100.11249346475344!3d-0.6253417995437778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd4e2fcb7497303%3A0xe94c5e99bef3f23c!2sGandoriah+Beach!5e0!3m2!1sen!2sid!4v1564481055534!5m2!1sen!2sid', NULL, 'tabuik1.png', 'tabuik2.jpg', 'tabuik3.jpg', '2019-07-30 03:11:44', '2019-07-30 03:11:44');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `nama_kat` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kat`, `created_at`, `updated_at`) VALUES
(1, 'Wisata Alam', '2019-05-25 22:55:13', '2019-05-25 22:55:13'),
(2, 'Wisata Kuliner', '2019-05-25 23:18:46', '2019-07-23 08:44:57'),
(3, 'Wisata Edukasi', '2019-05-26 00:15:43', '2019-07-11 00:28:45'),
(4, 'Wisata Budaya', '2019-06-11 07:58:56', '2019-06-11 07:58:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_26_040623_informasi', 1),
(4, '2019_05_26_041133_kategori', 1),
(5, '2019_05_26_041523_polling', 1),
(6, '2019_05_26_041721_pesan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('arumky.shalima@gmail.com', '$2y$10$YhmSE4/IObfUGQHAA0G6gutfU9OongxAbLnN6Q7AKPC1bYTu231L2', '2019-07-16 02:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `nama` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validation` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `nama`, `email`, `pesan`, `validation`, `created_at`, `updated_at`) VALUES
(1, 'Anggiet Bracmatya', 'anggiet.bracmatya@gmail.com', 'haloo kenalan dong hehehe', 0, '2019-06-02 02:52:08', '2019-06-02 02:52:08'),
(2, 'Anggiet Bracmatya', 'anggiet.bracmatya@gmail.com', 'haloo kenalan dong hehehe', 0, '2019-06-02 02:52:39', '2019-07-29 22:04:37'),
(3, 'Budiyanti', 'budiyanti@yahoo.com', 'kjqwieuqudabnsjkdkwjhafamsjhdfuiwelcsnkvswh jk nmdbcfhjgfbwc fakej jkwdr, fcwd', 1, '2019-07-03 03:54:24', '2019-07-19 23:16:11'),
(4, 'Budiyanti', 'budiyanti@yahoo.com', 'kjqwieuqudabnsjkdkwjhafamsjhdfuiwelcsnkvswh jk nmdbcfhjgfbwc fakej jkwdr fcwd', 0, '2019-07-03 03:54:36', '2019-07-03 03:54:36'),
(5, 'Budiyanti', 'budiyanti@yahoo.com', 'kjqwieuqudabnsjkdkwjhafamsjhdfuiwelcsnkvswh jk nmdbcfhjgfbwc fakej jkwdr fcwd', 0, '2019-07-03 03:54:51', '2019-07-03 03:54:51'),
(6, 'Friska', 'friskaap@gmail.com', 'Sate padang mak syukurnya enak!', 0, '2019-12-04 03:03:24', '2019-12-04 03:03:24');

-- --------------------------------------------------------

--
-- Table structure for table `polling`
--

CREATE TABLE `polling` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `sangat_baik` int(3) NOT NULL,
  `baik` int(3) NOT NULL,
  `cukup` int(3) NOT NULL,
  `kurang` int(3) NOT NULL,
  `total_poll` int(3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `polling`
--

INSERT INTO `polling` (`id`, `sangat_baik`, `baik`, `cukup`, `kurang`, `total_poll`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 2, 2, 9, NULL, '2019-07-12 21:51:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'arumky.shalima@gmail.com', NULL, '$2y$10$YhIWUEMmrjAIi2zAaFd6quiDITRZS7ilngMR6nZ7gJDYta3SS6ywG', 'JqbbD01N1mmLBj3R8AsDfmPUNGdswZksn23DPv4rD2sWCyE7gkFHSw2uHE7o', '2019-05-25 21:55:41', '2019-05-25 21:55:41'),
(2, 'Arsha', 'admin@gmail.com', NULL, '$2y$10$YhIWUEMmrjAIi2zAaFd6quiDITRZS7ilngMR6nZ7gJDYta3SS6ywG', NULL, '2019-07-16 06:53:23', '2019-07-16 06:53:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `polling`
--
ALTER TABLE `polling`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `informasi`
--
ALTER TABLE `informasi`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `polling`
--
ALTER TABLE `polling`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
