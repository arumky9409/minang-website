<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pesan;
use App\Informasi;
use App\Polling;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function polling(Request $request)
    {
        $polling = Polling::find($request->id);

        if($request->get('polling')=="Kurang") {
            $hasil_poling = $polling->kurang;
            $hasil_poling = $hasil_poling+1;
            $polling->kurang = $hasil_poling;
        }
        if($request->get('polling')=="Cukup") {
            $hasil_poling = $polling->cukup;
            $hasil_poling = $hasil_poling+1;
            $polling->cukup = $hasil_poling;
        }
        if($request->get('polling')=="Baik") {
            $hasil_poling = $polling->baik;
            $hasil_poling = $hasil_poling+1;
            $polling->baik = $hasil_poling;
        }
        if($request->get('polling')=="Sangat Baik") {
            $hasil_poling = $polling->sangat_baik;
            $hasil_poling = $hasil_poling+1;
            $polling->sangat_baik = $hasil_poling;
        }
            $total = $polling->total_poll;
            $total = $total+1;
            $polling->total_poll = $total;

            $polling->save();

            return redirect()->back();
    }

    public function showpesan()
    {
        $data['background'] = true;
        $data['title'] = "Kontak";
        return view('frontend.kritik-saran.index',$data);
    }

    public function indexalam()
    {
        $data['background'] = true;
        $data['alam'] = Informasi::all()->where('id_kategori','=','1');
        $data['title'] = "Wisata Alam";
        return view('frontend.alam',$data);
    }

    public function indexkuliner()
    {
        $data['background'] = true;
        $data['kuliner'] = Informasi::all()->where('id_kategori','=','2');
        $data['title'] = "Wisata Kuliner";
        return view('frontend.kuliner',$data);
    }

    public function indexedu()
    {
        $data['background'] = true;
        $data['edu'] = Informasi::all()->where('id_kategori','=','3');
        $data['title'] = "Wisata Edukasi";
        return view('frontend.edukasi',$data);
    }

    public function indexbudaya()
    {
        $data['background'] = true;
        $data['budaya'] = Informasi::all()->where('id_kategori','=','4');
        $data['title'] = "Wisata Budaya";
        return view('frontend.budaya',$data);
    }

    public function detail($id)
    {
        $data['background'] = true;
        $data['wisata'] = Informasi::find($id);
        $data['title'] = "Detail Wisata ";
        return view('frontend.details',$data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pesan = new Pesan;
        $pesan->nama = $request->nama;
        $pesan->email = $request->email;
        $pesan->pesan = $request->pesan;
        $pesan->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
