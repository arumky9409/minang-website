<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Informasi;
use App\Kategori;
use App\Http\Controllers\Auth;

class InformasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $informasis = Informasi::paginate(8);
        return view('admin.informasi.index', compact('informasis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoris = Kategori::All();
        return view('admin.informasi.create', compact('kategoris'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategoris = Kategori::All();
        $inputinfo = New Informasi;
        $inputinfo->judul = $request->judul;
        $inputinfo->id_kategori = $request->id_kategori;
        $inputinfo->jalan = $request->jalan;
        $inputinfo->kelkec = $request->kelkec;
        $inputinfo->kotakab = $request->kotakab;
        $inputinfo->provinsi = $request->provinsi;
        $inputinfo->konten = $request->konten;
        $inputinfo->peta = $request->peta;
        $gambar1 = $request->foto1;
            $namagambar1 = $gambar1->getClientOriginalName();
            $gambar1->move('images', $namagambar1);
        $inputinfo->foto1 = $namagambar1;
        $gambar2 = $request->foto2;
            $namagambar2 = $gambar2->getClientOriginalName();
            $gambar2->move('images', $namagambar2);
        $inputinfo->foto2 = $namagambar2;
        $gambar3 = $request->foto3;
            $namagambar3 = $gambar3->getClientOriginalName();
            $gambar3->move('images', $namagambar3);
        $inputinfo->foto3 = $namagambar3;
        $inputinfo->save();
        return view('admin.informasi.create', compact('kategoris'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $id;        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $id;
    }
}
