<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Informasi;
use App\Kategori;

class InformasiController extends Controller
{
     
    public function index()
    {
        $informasis = Informasi::paginate(8);
        return view('admin.informasi.index', compact('informasis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoris = Kategori::All();
        return view('admin.informasi.create', compact('kategoris'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategoris = Kategori::All();
        $inputinfo = New Informasi;
        $inputinfo->judul = $request->judul;
        $inputinfo->id_kategori = $request->id_kategori;
        $inputinfo->jalan = $request->jalan;
        $inputinfo->kelkec = $request->kelkec;
        $inputinfo->kotakab = $request->kotakab;
        $inputinfo->provinsi = $request->provinsi;
        $inputinfo->konten = $request->konten;
        $inputinfo->peta = $request->peta;

        $gambar1 = $request->foto1;
            $namagambar1 = $gambar1->getClientOriginalName();
            $gambar1->move('images', $namagambar1);
        $inputinfo->foto1 = $namagambar1;

        $gambar2 = $request->foto2;
            $namagambar2 = $gambar2->getClientOriginalName();
            $gambar2->move('images', $namagambar2);
        $inputinfo->foto2 = $namagambar2;
        
        $gambar3 = $request->foto3;
            $namagambar3 = $gambar3->getClientOriginalName();
            $gambar3->move('images', $namagambar3);
        $inputinfo->foto3 = $namagambar3;
        $inputinfo->save();
        return view('admin.informasi.create', compact('kategoris'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kategoris'] = Kategori::All();
        $data['informasi'] = Informasi::find($id);
        return view('admin.informasi.edit',$data);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateInformasi =  \App\Informasi::findOrFail($id);
        $updateInformasi->judul = $request->get('judul');
        $updateInformasi->id_kategori = $request->get('id_kategori');
        $updateInformasi->provinsi = $request->get('provinsi');
        $updateInformasi->kotakab = $request->get('kotakab');
        $updateInformasi->kelkec = $request->get('kelkec');
        $updateInformasi->konten = $request->get('konten');
        $updateInformasi->peta = $request->get('peta');
        $updateInformasi->jalan = $request->get('jalan');
        if($request->populer) {
            $inputstatus = $request->populer;
            $updateInformasi->status = $inputstatus;
        }

        $gambar1 = $request->foto1;
            $namagambar1 = $gambar1->getClientOriginalName();
            $gambar1->move('images', $namagambar1);
        $updateInformasi->foto1 = $namagambar1;


        $gambar2 = $request->foto2;
            $namagambar2 = $gambar2->getClientOriginalName();
            $gambar2->move('images', $namagambar2);
        $updateInformasi->foto2 = $namagambar2;

        $gambar3 = $request->foto3;
            $namagambar3 = $gambar3->getClientOriginalName();
            $gambar3->move('images', $namagambar3);
        $updateInformasi->foto3 = $namagambar3;
        $updateInformasi->save();

        return redirect('informasi');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $informasi = Informasi::findOrFail($id);
        $informasi->delete();
        return back();
    }
}
