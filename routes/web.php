<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data['background'] = false;
    $data['title'] = "Wisata Minang";
    return view('welcome',$data);
});

Auth::routes();


Route::resource('coba','CobaController');
Route::resource('informasi','InformasiController');
Route::resource('kategori','KategoriController');
Route::resource('polling','PollingController');


Route::get('/admin/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/detail/{id}', 'FrontendController@detail')->name('detail');

Route::prefix('admin/kategori')->namespace('AdminController')->group(function() 
{
    Route::get('/create', 'KategoriController@create')->name('createKategori');
    Route::post('/create','KategoriController@store')->name('storeKategori');
    Route::get('/index','KategoriController@index')->name('indexKategori');
});


Route::prefix('admin/informasi')->namespace('AdminController')->group(function()
{
    Route::get('/create','InformasiController@create')->name('createInformasi');
    Route::post('/create','InformasiController@store')->name('storeInformasi');
    Route::get('/index','InformasiController@index')->name('indexInformasi');
    Route::get('/edit','InformasiController@edit')->name('editInformasi');
});


Route::prefix('admin/pesan')->namespace('AdminController')->group(function()
{
    Route::get('/index','PesanController@index')->name('indexPesan');
    Route::get('/validation/{id}', 'PesanController@validation')->name('validation');
    Route::get('/validationNo/{id}', 'PesanController@validationNo')->name('validationNo');
    Route::get('/{id}', 'PesanController@destroy')->name('hapusPesan');

});

Route::prefix('frontend/kontak')->group(function()
{
	Route::get('','FrontendController@showpesan')->name('showPesan');
	Route::post('', 'FrontendController@store')->name('storePesan');
});

Route::get('/frontend/alam', 'FrontendController@indexalam')->name('indexAlam');

Route::get('/frontend/kuliner', 'FrontendController@indexkuliner')->name('indexKuliner');

Route::get('/frontend/edukasi', 'FrontendController@indexedu')->name('indexEdu');

Route::get('/frontend/budaya', 'FrontendController@indexbudaya')->name('indexBudaya');

Route::post('/welcome/{succes}', 'FrontendController@polling')->name('storePolling');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
